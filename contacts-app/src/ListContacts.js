import React, { Component } from 'react';
// import React from 'react';
import PropTypes from 'prop-types';

// STYLE IMPORT
import './list_contact.css';

// ---------------------------------------------------------
// ----------- EXAMPLE OF THIS USE TO CALL PROPS -----------
// ---------------------------------------------------------
class ListContacts extends Component {
	static propTypes = {
		contacts: PropTypes.array.isRequired,
		removeMethod: PropTypes.func.isRequired,
	}
	state = {
		query: ''
	}

	// method to search by input
	updateQuery = (query) => {
		this.setState(() => ({
			query: query.trim()
		}))
	}

	// method to clear the search bar
	clearQuery = () => {
		this.updateQuery('')
	}

	render() {
		const { query } = this.state
		const { contacts, removeMethod } = this.props

		const shownContacts = query === ''
			? contacts
			: contacts.filter((c) => (
				c.name.toLowerCase().includes(query.toLowerCase())
			))

		// console.log('Props', this.props);
		return(
			<div className="list-contacts">
				{JSON.stringify(this.state)}
				<div className="input-search">
					<input 
						className="search-bar"
						type="text"
						placehoder="Search contacts"
						value={query}
						onChange={(event) => this.updateQuery(event.target.value)}
					/>
					{shownContacts.length !== contacts.length && (
						<div className="found-number">
							<div className="search-length">{shownContacts.length} found</div>
							<button className="clear-button" onClick={this.clearQuery}>Clear</button>
						</div>
					)}
				</div>
				<ul className="contacts-list">
					{shownContacts.map((contact) => (
						<li key={contact.id}>
							<div className="contact-photo">
								<div className="photo" style={{backgroundImage: `url(${contact.avatarURL})`}}></div>
							</div>
							<div className="contact-area">
								<div className="info">
									<p className="name">{contact.name}</p>
									<p className="item">@{contact.handle}</p>
								</div>
							</div>
							<div className="remove-area" onClick={() => removeMethod(contact)}>
								<i className="fas fa-times-circle"></i>
							</div>
						</li>
					))}
				</ul>
			</div>
		)
	}
}

// const ListContacts = (props) => (
// 	<ul className="contacts-list">
// 		{ props.contacts.map((contact) => (
// 			<li key={contact.id}>
// 				<div className="contact-photo">
// 					<div className="photo" style={{backgroundImage: `url(${contact.avatarURL})`}}></div>
// 				</div>
// 				<div className="contact-area">
// 					<div className="info">
// 						<p className="name">{contact.name}</p>
// 						<p className="link">@{contact.handle}</p>
// 					</div>
// 				</div>
// 				<div className="remove-area" onClick={() => props.removeMethod(contact)}>
// 					<i className="fas fa-times-circle"></i>
// 				</div>
// 			</li>
// 		))}
// 	</ul>
// );

// ListContacts.protoTypes = {
// 	contacts: PropTypes.array.isRequired,
// 	removeMethod: PropTypes.func.isRequired,
// }


// ---------------------------------------------------------
// ---------------- STATELESS COMPONENT -------------------
// ---------------------------------------------------------
// function ListContacts(props) {
// 	return (
// 		<ul className="contacts-list">
// 			{ props.contacts.map((contact) => (
// 				<li key={contact.id}>
// 					<div className="contact-photo">
// 						<div className="photo" style={{backgroundImage: `url(${contact.avatarURL})`}}></div>
// 					</div>
// 					<div className="contact-area">
// 						<div className="info">
// 							<p className="name">{contact.name}</p>
// 							<p className="item">@{contact.handle}</p>
// 						</div>
// 					</div>
// 					<div className="close-icon">
// 						<i className="fas fa-times-circle"></i>
// 					</div>
// 				</li>
// 			))}
// 		</ul>
// 	)
// }

export default ListContacts