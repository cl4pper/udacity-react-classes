import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class ContactList extends Component {
  render() {
    const contacts = this.props.contacts;
    return (
      <ul>
        {contacts.map(contact => (
          <li key={contact.name}>{contact.name}</li>
        ))}
      </ul>
    );
  }
}

class App extends Component {
  render() {
    return (
      <div className="app">
        <ContactList contacts={ [{ name: 'Wall' }, { nmae: 'Xiko' }] } />
        <ContactList contacts={ [{ name: 'James' }, { name: 'Will' }] } />
        <ContactList contacts={ [{ name: 'Matts' }] }/>
      </div>
    );
  }
}

export default App;
